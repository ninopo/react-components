import PropTypes from 'prop-types';

export default function Photos({url, title}){
  return (
     <img src={url} alt={title}/>
   )
}

Photos.propTypes = {
    url: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired
}

