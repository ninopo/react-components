import PropTypes from 'prop-types';
import './Footer.css';

export default function Footer({title="Footer title"}){
    return (
        <h2>{title}</h2>
    )
}

Footer.propTypes = {
    title: PropTypes.string.isRequired
}