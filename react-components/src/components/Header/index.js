import PropTypes from 'prop-types';
import './Header.css';

export default function Header({title="Header title"}){
   return (
    <h1>{title}</h1>
   )
}

Header.propTypes = {
    title: PropTypes.string.isRequired
}