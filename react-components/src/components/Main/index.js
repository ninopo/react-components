import Photos from "../Photos";


export default function Main(props){

    const styles={
        backgroundColor:'rgb(133, 133, 133)',
        width:'100%',
        padding:'100px 0'
    }
   
    return(
        <main style={styles}
        >{props.children}</main>
    )
}